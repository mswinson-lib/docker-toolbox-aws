.PHONY: build clean smoke deploy release

IMAGE_REPO=mswinson
IMAGE_NAME=toolbox-aws
ID=$(IMAGE_REPO)/$(IMAGE_NAME)
TAG=python2.7-alpine3.8
VERSION=`cat VERSION`
TAG_SUFFIX ?= '-develop'

build:
	docker build -t $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX) .

clean:
	docker rmi $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX)

smoke: build
	docker run --rm -v $(PWD):/var/workspace --entrypoint /bin/sh $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX) -c ./test/smoke.sh

deploy:
	docker login --username $(DOCKER_USER) --password $(DOCKER_PASS)
	docker push $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX)

release:
	docker tag $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX) $(ID):latest
	docker tag $(ID):$(TAG)-$(VERSION)$(TAG_SUFFIX) $(ID):$(TAG)
	docker login --username $(DOCKER_USER) --password $(DOCKER_PASS)
	docker push $(ID):latest
	docker push $(ID):$(TAG)
