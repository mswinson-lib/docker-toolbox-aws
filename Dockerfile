FROM python:2.7-alpine3.8
MAINTAINER Mark Swinson <mark@mswinson.com>

RUN mkdir -p /var/tmp/packer-build
ADD source /var/tmp/packer-build
RUN chmod u+x /var/tmp/packer-build/setup.sh && \
    /var/tmp/packer-build/setup.sh

RUN mkdir -p /var/workspace
WORKDIR /var/workspace
ENTRYPOINT ["/bin/sh", "-c"]
CMD ["/bin/bash"]
